# IndustrialCraft 2 Nuclear Control #

IndustrialCraft 2 Nuclear Control is an addon for the IndustrialCraft 2 mod for Minecraft. This addon allows you to build efficient monitoring and notification system for your nuclear reactor. Also you can use Howler Alarm and Industrial Alarm in any case when you want industrial-style notification/alarming system.

## Credits ##

Shedar: Author, Coder

xbony2: Coder, Design

Zuxelus: Coder

### Forum ###

Original thread

http://forum.industrial-craft.net/index.php?page=Thread&threadID=5915

Nuclear Control 2 (from version 1.7.2)

http://forum.industrial-craft.net/index.php?page=Thread&threadID=10649

### Changelog ###

[Changelog for version 1.7.10](https://bitbucket.org/Zuxelus/ic2-nuclear-control/wiki/Changelog)

### Downloads ###
Version IC2NuclearControl-1.6.2b.zip for IC2 1.118.401-lf (Minecraft 1.6.4)

[IC2NuclearControl-1.6.2b.zip](http://forum.industrial-craft.net/index.php?page=Attachment&attachmentID=3296&h=6c15c8beea5376f643dc4b4504319d217ec90fe0)

Version IC2NuclearControl-1.6.2e-ic2-experimental.zip for IC2 2.0.397-experimental (Minecraft 1.6.4)

[IC2NuclearControl-1.6.2e-ic2-experimental.zip](http://forum.industrial-craft.net/index.php?page=Attachment&attachmentID=3556&h=ba95ef71b59398362302733d85382b2310726012)

Version ic2nc-1.7.2-1.7.2.08.jar for IC2 2.1.484-experimental (Minecraft 1.7.2)

[ic2nc-1.7.2-1.7.2.08.jar](https://bitbucket.org/Zuxelus/ic2-nuclear-control/downloads/ic2nc-1.7.2-1.7.2.08.jar)

Version ic2nc-1.7.10-1.7.10.07.jar (Minecraft 1.7.10)

[ic2nc-1.7.10-1.7.10.07.jar](https://bitbucket.org/Zuxelus/ic2-nuclear-control/downloads/ic2nc-1.7.10-1.7.10.07.jar)