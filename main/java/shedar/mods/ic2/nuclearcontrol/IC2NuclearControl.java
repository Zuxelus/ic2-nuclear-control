package shedar.mods.ic2.nuclearcontrol;

import ic2.api.item.IC2Items;
import ic2.api.recipe.Recipes;
import ic2.core.util.StackUtil;

import java.util.EnumMap;
import java.util.List;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;

import org.apache.logging.log4j.Logger;

import shedar.mods.ic2.nuclearcontrol.blocks.BlockNuclearControlMain;
import shedar.mods.ic2.nuclearcontrol.crossmod.buildcraft.CrossBuildcraft;
//import shedar.mods.ic2.nuclearcontrol.crossmod.gregtech.CrossGregTech;
import shedar.mods.ic2.nuclearcontrol.crossmod.ic2.CrossIndustrialCraft2;
import shedar.mods.ic2.nuclearcontrol.crossmod.railcraft.CrossRailcraft;
import shedar.mods.ic2.nuclearcontrol.items.ItemCardEnergyArrayLocation;
import shedar.mods.ic2.nuclearcontrol.items.ItemCardEnergySensorLocation;
import shedar.mods.ic2.nuclearcontrol.items.ItemCardLiquidArrayLocation;
import shedar.mods.ic2.nuclearcontrol.items.ItemCardMultipleSensorLocation;
import shedar.mods.ic2.nuclearcontrol.items.ItemCardReactorSensorLocation;
import shedar.mods.ic2.nuclearcontrol.items.ItemCardText;
import shedar.mods.ic2.nuclearcontrol.items.ItemKitEnergySensor;
import shedar.mods.ic2.nuclearcontrol.items.ItemKitMultipleSensor;
import shedar.mods.ic2.nuclearcontrol.items.ItemKitReactorSensor;
import shedar.mods.ic2.nuclearcontrol.items.ItemNuclearControlMain;
import shedar.mods.ic2.nuclearcontrol.items.ItemTimeCard;
import shedar.mods.ic2.nuclearcontrol.items.ItemToolDigitalThermometer;
import shedar.mods.ic2.nuclearcontrol.items.ItemToolThermometer;
import shedar.mods.ic2.nuclearcontrol.items.ItemUpgrade;
import shedar.mods.ic2.nuclearcontrol.network.ChannelHandler;
import shedar.mods.ic2.nuclearcontrol.panel.ScreenManager;
import shedar.mods.ic2.nuclearcontrol.utils.Damages;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.FMLEmbeddedChannel;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;

@Mod( modid = "IC2NuclearControl", name="Nuclear Control", version="1.7.2.08", guiFactory = "shedar.mods.ic2.nuclearcontrol.gui.GuiFactory", dependencies ="after:IC2")
public class IC2NuclearControl
{

	public static final int COLOR_WHITE = 15;
	public static final int COLOR_ORANGE = 14;
	public static final int COLOR_MAGENTA = 13;
	public static final int COLOR_LIGHT_BLUE = 12;
	public static final int COLOR_YELLOW = 11;
	public static final int COLOR_LIME = 10;
	public static final int COLOR_PINK = 9;
	public static final int COLOR_GRAY = 8;
	public static final int COLOR_LIGHT_GRAY = 7;
	public static final int COLOR_CYAN = 6;
	public static final int COLOR_PURPLE = 5;
	public static final int COLOR_BLUE = 4;
	public static final int COLOR_BROWN = 3;
	public static final int COLOR_GREEN = 2;
	public static final int COLOR_RED = 1;
	public static final int COLOR_BLACK = 0;

	// The instance of your mod that Forge uses.
	@Instance
	public static IC2NuclearControl instance;

	// Says where the client and server 'proxy' code is loaded.
	@SidedProxy(clientSide = "shedar.mods.ic2.nuclearcontrol.ClientProxy", serverSide = "shedar.mods.ic2.nuclearcontrol.CommonProxy")
	
	//The proxy to be used by client and server
	public static CommonProxy proxy;
	
	//Mod's creative tab
	public static IC2NCCreativeTabs tabIC2NC = new IC2NCCreativeTabs();

	//Mod log
	public static Logger logger;
	public static ConfigurationHandler config;

	public String allowedAlarms;
	public List<String> serverAllowedAlarms;
	public Item itemToolThermometer;
	public Item itemToolDigitalThermometer;
	public Item itemRemoteSensorKit;
	public Item itemEnergySensorKit;
	public Item itemMultipleSensorKit;
	public Item itemSensorLocationCard;
	public Item itemEnergySensorLocationCard;
	public Item itemMultipleSensorLocationCard;
	public Item itemEnergyArrayLocationCard;
	public Item itemLiquidArrayLocationCard;
	public Item itemTimeCard;
	public Item itemUpgrade;
	public Item itemTextCard;
	public BlockNuclearControlMain blockNuclearControlMain;
	public int modelId;
	public int alarmRange;
	public int SMPMaxAlarmRange;
	public int maxAlarmRange;
	public boolean isHttpSensorAvailable;
	public String httpSensorKey;
	public List<String> availableAlarms;
	public int remoteThermalMonitorEnergyConsumption;
	public ScreenManager screenManager = new ScreenManager();
	public int screenRefreshPeriod;
	public int rangeTriggerRefreshPeriod;

	public CrossBuildcraft crossBC;
	public CrossIndustrialCraft2 crossIC2;
	//public CrossGregTech crossGregTech;
	public CrossRailcraft crossRailcraft;

	@SuppressWarnings("unchecked")
	protected void addRecipes()
	{
		ItemStack thermalMonitor = new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_THERMAL_MONITOR);
		Recipes.advRecipes.addRecipe(thermalMonitor, new Object[]
				{
				"GGG", "GCG", "GRG", 
				Character.valueOf('G'), IC2Items.getItem("reinforcedGlass"), 
				Character.valueOf('R'), Items.redstone, 
				Character.valueOf('C'), IC2Items.getItem("advancedCircuit")
				});
		ItemStack howler = new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_HOWLER_ALARM);
		Recipes.advRecipes.addRecipe(howler, new Object[]
				{
				"NNN", "ICI", "IRI", 
				Character.valueOf('I'), Items.iron_ingot, 
				Character.valueOf('R'), Items.redstone, 
				Character.valueOf('N'), Blocks.jukebox, 
				Character.valueOf('C'), IC2Items.getItem("electronicCircuit")
				});
		ItemStack industrialAlarm = new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_INDUSTRIAL_ALARM);
		Recipes.advRecipes.addRecipe(industrialAlarm, new Object[]
				{
				"GOG", "GHG", "GRG", 
				Character.valueOf('G'), IC2Items.getItem("reinforcedGlass"), 
				Character.valueOf('O'), "dyeOrange", 
				Character.valueOf('R'), Items.redstone, 
				Character.valueOf('H'), howler 
				});

		Recipes.advRecipes.addRecipe(new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_REMOTE_THERMO), new Object[] 
				{
			"F", "M", "T", 
			Character.valueOf('T'), thermalMonitor, 
			Character.valueOf('M'), IC2Items.getItem("machine"), 
			Character.valueOf('F'), IC2Items.getItem("frequencyTransmitter")
				});
		Recipes.advRecipes.addRecipe(new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_INFO_PANEL), new Object[] 
				{
			"PPP", "LCL", "IRI", 
			Character.valueOf('P'), Blocks.glass_pane, 
			Character.valueOf('L'), "dyeLime", 
			Character.valueOf('I'), "dyeBlack", 
			Character.valueOf('R'), Items.redstone, 
			Character.valueOf('C'), IC2Items.getItem("electronicCircuit") 
				});
		Recipes.advRecipes.addRecipe(new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_INFO_PANEL_EXTENDER), new Object[] 
				{
			"PPP", "WLW", "WWW", 
			Character.valueOf('P'), Blocks.glass_pane, 
			Character.valueOf('L'), "dyeLime", 
			Character.valueOf('W'), Blocks.planks, 
				});
		Recipes.advRecipes.addRecipe(new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_ADVANCED_PANEL), new Object[] 
				{
			"PPP", "GLG", "CAC", 
			Character.valueOf('P'), Blocks.glass_pane, 
			Character.valueOf('L'), "dyeLime", 
			Character.valueOf('G'), IC2Items.getItem("goldCableItem"),
			Character.valueOf('A'), IC2Items.getItem("advancedCircuit"), 
			Character.valueOf('C'), IC2Items.getItem("carbonPlate") 
				});
		Recipes.advRecipes.addRecipe(new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_ADVANCED_EXTENDER), new Object[] 
				{
			"PPP", "GLG", "GCG", 
			Character.valueOf('P'), Blocks.glass_pane, 
			Character.valueOf('L'), "dyeLime", 
			Character.valueOf('G'), IC2Items.getItem("goldCableItem"),
			Character.valueOf('C'), IC2Items.getItem("carbonPlate") 
				});
		Recipes.advRecipes.addRecipe(new ItemStack(itemToolThermometer, 1), new Object[] 
				{
			"IG ", "GWG", " GG", 
			Character.valueOf('G'), Blocks.glass, 
			Character.valueOf('I'), Items.iron_ingot, 
			Character.valueOf('W'), IC2Items.getItem("waterCell")
				});
		ItemStack digitalThermometer = new ItemStack(itemToolDigitalThermometer, 1);
		Recipes.advRecipes.addRecipe(digitalThermometer, new Object[] 
				{
				"I  ", "IC ", " GI", 
				Character.valueOf('G'), Items.glowstone_dust, 
				Character.valueOf('I'), Items.iron_ingot, 
				Character.valueOf('C'), IC2Items.getItem("electronicCircuit")
				});
		Recipes.advRecipes.addRecipe(new ItemStack(itemRemoteSensorKit, 1), new Object[] 
				{
			"  F", " D ", "P  ", 
			Character.valueOf('P'), Items.paper, 
			Character.valueOf('D'), StackUtil.copyWithWildCard(digitalThermometer), 
			Character.valueOf('F'), IC2Items.getItem("frequencyTransmitter")
				});
		Recipes.advRecipes.addRecipe(new ItemStack(itemEnergySensorKit, 1), new Object[] 
				{
			"  F", " D ", "P  ", 
			Character.valueOf('P'), Items.paper, 
			Character.valueOf('D'), IC2Items.getItem("ecMeter"), 
			Character.valueOf('F'), IC2Items.getItem("frequencyTransmitter")
				});
		Recipes.advRecipes.addRecipe(new ItemStack(itemUpgrade, 1, ItemUpgrade.DAMAGE_RANGE), new Object[] 
				{
			"CFC", 
			Character.valueOf('C'), IC2Items.getItem("insulatedCopperCableItem"), 
			Character.valueOf('F'), IC2Items.getItem("frequencyTransmitter")
				});
		Recipes.advRecipes.addRecipe(new ItemStack(itemUpgrade, 1, ItemUpgrade.DAMAGE_COLOR), new Object[] 
				{
			"RYG","WCM","IAB", 
			Character.valueOf('R'), "dyeRed",  
			Character.valueOf('Y'), "dyeYellow",  
			Character.valueOf('G'), "dyeGreen",  
			Character.valueOf('W'), "dyeWhite",  
			Character.valueOf('C'), IC2Items.getItem("insulatedCopperCableItem"), 
			Character.valueOf('M'), "dyeMagenta",  
			Character.valueOf('I'), "dyeBlack",  
			Character.valueOf('A'), "dyeCyan",  
			Character.valueOf('B'), "dyeBlue"  
				});
		if(isHttpSensorAvailable)
		{
			Recipes.advRecipes.addRecipe(new ItemStack(itemUpgrade, 1, ItemUpgrade.DAMAGE_WEB), new Object[] 
					{
				"CFC","CAC","CFC", 
				Character.valueOf('C'), new ItemStack(itemUpgrade, 1, ItemUpgrade.DAMAGE_RANGE), 
				Character.valueOf('A'), IC2Items.getItem("advancedCircuit"),
				Character.valueOf('F'), IC2Items.getItem("glassFiberCableItem")
					});
		}

		ItemStack energyCounter = new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_ENERGY_COUNTER);
		Recipes.advRecipes.addRecipe(energyCounter, new Object[]
				{
				" A ", "FTF", 
				Character.valueOf('A'), IC2Items.getItem("advancedCircuit"), 
				Character.valueOf('F'), IC2Items.getItem("glassFiberCableItem"), 
				Character.valueOf('T'), IC2Items.getItem("mvTransformer")
				});
		ItemStack averageCounter = new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_AVERAGE_COUNTER);
		Recipes.advRecipes.addRecipe(averageCounter, new Object[]
				{
				"FTF", " A ",  
				Character.valueOf('A'), IC2Items.getItem("advancedCircuit"), 
				Character.valueOf('F'), IC2Items.getItem("glassFiberCableItem"), 
				Character.valueOf('T'), IC2Items.getItem("mvTransformer")
				});
		ItemStack rangeTrigger = new ItemStack(blockNuclearControlMain, 1, Damages.DAMAGE_RANGE_TRIGGER);
		Recipes.advRecipes.addRecipe(rangeTrigger, new Object[]
				{
				"EFE", "AMA",  " R ",

				Character.valueOf('E'), IC2Items.getItem("detectorCableItem"), 
				Character.valueOf('F'), IC2Items.getItem("frequencyTransmitter"),
				Character.valueOf('A'), IC2Items.getItem("advancedCircuit"), 
				Character.valueOf('M'), IC2Items.getItem("machine"), 
				Character.valueOf('R'), Items.redstone 
				});
		Recipes.advRecipes.addRecipe(new ItemStack(itemMultipleSensorKit, 1, ItemKitMultipleSensor.TYPE_COUNTER), new Object[] 
				{
			"  F", " C ", "P  ", 
			Character.valueOf('P'), Items.paper, 
			Character.valueOf('C'), IC2Items.getItem("electronicCircuit"), 
			Character.valueOf('F'), IC2Items.getItem("frequencyTransmitter")
				});
		Recipes.advRecipes.addRecipe(new ItemStack(itemMultipleSensorKit, 1, ItemKitMultipleSensor.TYPE_LIQUID), new Object[] 
				{
			"  F", " C ", "P  ", 
			Character.valueOf('P'), Items.paper, 
			Character.valueOf('C'), Items.bucket, 
			Character.valueOf('F'), IC2Items.getItem("frequencyTransmitter")
				});
		Recipes.advRecipes.addRecipe(new ItemStack(itemMultipleSensorKit, 1, ItemKitMultipleSensor.TYPE_GENERATOR), new Object[]
				{
			"  F", " D ", "P  ", 
			Character.valueOf('P'), Items.paper, 
			Character.valueOf('D'), IC2Items.getItem("energyStorageUpgrade"), 
			Character.valueOf('F'), IC2Items.getItem("frequencyTransmitter")
				});
		Recipes.advRecipes.addRecipe(new ItemStack(itemTextCard, 1), new Object[] 
				{
			" C ", "PFP", " C ", 
			Character.valueOf('P'), Items.paper, 
			Character.valueOf('C'), IC2Items.getItem("electronicCircuit"), 
			Character.valueOf('F'), IC2Items.getItem("insulatedCopperCableItem")
				});
		Recipes.advRecipes.addShapelessRecipe(new ItemStack(itemTimeCard, 1),  
				IC2Items.getItem("electronicCircuit"), Items.clock);
		Recipes.advRecipes.addShapelessRecipe(new ItemStack(IC2Items.getItem("electronicCircuit").getItem(), 2),  
				itemSensorLocationCard );
		Recipes.advRecipes.addShapelessRecipe(new ItemStack(IC2Items.getItem("electronicCircuit").getItem(), 2),  
				itemEnergySensorLocationCard );
		Recipes.advRecipes.addShapelessRecipe(new ItemStack(IC2Items.getItem("electronicCircuit").getItem(), 2),  
				new  ItemStack(itemMultipleSensorLocationCard, 1, ItemKitMultipleSensor.TYPE_COUNTER));
		Recipes.advRecipes.addShapelessRecipe(new ItemStack(IC2Items.getItem("electronicCircuit").getItem(), 1),  
				new  ItemStack(itemMultipleSensorLocationCard, 1, ItemKitMultipleSensor.TYPE_LIQUID));
		CraftingManager.getInstance().getRecipeList().add(new StorageArrayRecipe());
	}

	protected void initBlocks()
	{
		blockNuclearControlMain = new BlockNuclearControlMain();
		itemToolThermometer = new ItemToolThermometer("ItemToolThermometer");
		itemToolDigitalThermometer = new ItemToolDigitalThermometer("ItemToolDigitalThermometer",1,80,80);
		itemSensorLocationCard = new ItemCardReactorSensorLocation("ItemSensorLocationCard");
		itemUpgrade = new ItemUpgrade("itemRangeUpgrade");
		itemTimeCard = new ItemTimeCard("ItemTimeCard");
		itemTextCard = new ItemCardText("ItemTextCard");
		itemEnergySensorLocationCard = new ItemCardEnergySensorLocation("ItemEnergySensorLocationCard");
		itemEnergyArrayLocationCard = new ItemCardEnergyArrayLocation("ItemEnergyArrayLocationCard");
		itemLiquidArrayLocationCard = new ItemCardLiquidArrayLocation("ItemLiquidArrayLocationCard");
		itemMultipleSensorLocationCard = new ItemCardMultipleSensorLocation("itemCounterSensorLocationCard");
		itemMultipleSensorKit = new ItemKitMultipleSensor("ItemCounterSensorKit");
		itemEnergySensorKit = new ItemKitEnergySensor("ItemEnergySensorKit");
		itemRemoteSensorKit = new ItemKitReactorSensor("ItemRemoteSensorKit");
	}

	@EventHandler
	public void modsLoaded(FMLPostInitializationEvent evt)
	{
		addRecipes();
	}

	public void registerBlocks()
	{
		GameRegistry.registerBlock(blockNuclearControlMain, ItemNuclearControlMain.class, "blockNuclearControlMain");
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) 
	{
		logger = event.getModLog();
		//Load configuration
		config = new ConfigurationHandler();
		FMLCommonHandler.instance().bus().register(config);
		config.init(event.getSuggestedConfigurationFile());

		//Register channel handler
		ChannelHandler.init();

		//Register event handlers
		MinecraftForge.EVENT_BUS.register(ServerTickHandler.instance);
		FMLCommonHandler.instance().bus().register(ServerTickHandler.instance);
		
		if (event.getSide().isClient())
		{
			MinecraftForge.EVENT_BUS.register(ClientTickHandler.instance);
			FMLCommonHandler.instance().bus().register(ClientTickHandler.instance);
		}
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, proxy);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent evt)
	{
		crossBC = new CrossBuildcraft();
		crossIC2 = new CrossIndustrialCraft2();
		//crossGregTech = new CrossGregTech();
		crossRailcraft = new CrossRailcraft();
	}

	@EventHandler
	public void init(FMLInitializationEvent evt)
	{
		IC2NuclearControl.instance.screenManager = new ScreenManager();
		initBlocks();
		registerBlocks();
		proxy.registerTileEntities();
	}
}
